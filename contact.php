<?php
require_once('assets/configuration.php');
$type = 'contact';
$name = '';
$sql = "SELECT title,description,canonical,css,js FROM pages WHERE type = '$type' AND name = ''";
$result = $conn->query($sql);
if($result->num_rows > 0){
while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html><!-- 30189 -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $row["title"];?>
<?php echo $row["description"];?>
<?php echo $row["canonical"];?>
<?php echo $row["css"];?>
</head>
<body>
<?php include 'assets/search.php';?>
<header class="header">
<?php include 'assets/topbar.php';?>


			<!--main navigation start-->
			<!-- Static navbar -->
			<nav class="navbar navbar-default navbar-static-top yamm sticky">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt="logo"></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<!--mega menu-->
							<li class="dropdown active yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Herculiner Bed Liner<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Base pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
														<li><a href="register.php"><i class="fa fa-user"></i>Register</a></li>
														<li><a href="contact.php"><i class="fa fa-map-marker"></i>Contact</a></li>
														<li><a href="404.php"><i class="fa fa-trash-o"></i>404</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Product Pages </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="product-list.php"><i class="fa fa-list"></i> Product list</a></li>
														<li><a href="product-detail.php"><i class="fa fa-angle-right"></i> Product Detail </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Cart Pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
														<li><a href="checkout.php"><i class="fa fa-truck"></i> Checkout</a></li>   
														<li><a href="order-history.php"><i class="fa fa-sliders"></i> Order history </a></li>
														<li><a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/10.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<!--mega menu-->
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Blue DEF Equipment<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/8.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Peak 12v Electronics<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#">Nam ipsum est</a></li>
									<li><a href="#">Volutpat</a></li>
									<li><a href="#">In efficitur in</a></li>
									<li><a href="#">Accumsan eget</a></li>
									<li><a href="#">Curabitur</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Blog<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">

									<li><a href="blog-masonry.php">Masonry view</a></li>
									<li><a href="blog-post.php">Single Post</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="pe-7s-cart"></i><span class="badge">5</span></a>
								<div class="dropdown-menu shopping-cart">
									<div class="cart-items content-scroll">
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/1.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/4.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/2.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/3.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/6.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
									</div><!--cart-items-->
									<div class="cart-footer">
										<a href="#" class="btn btn-light-dark">View Cart</a>
										<a href="#" class="btn btn-skin">Checkout</a>
									</div><!--footer of cart-->
								</div><!--cart dropdown end-->
							</li>

						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			<!--main navigation end-->
		</header>
		<!--header end-->

		<!--breadcrumb start-->
		<div class="breadcrumb-wrapper">
			<div class="container">
				<h1>Get in touch!</h1>
			</div>
		</div>
		<!--end breadcrumb-->

		<div class="space-60"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h3>We would love to hear from you. </h3>
					<p>
						Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimusNam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus
					</p>
					<form name="sentMessage" id="contactForm" method="post" novalidate="">
						<div class="row">
							<div class="col-md-6">
								<div class="row control-group">
									<div class="form-group col-xs-12 controls">
									
										<input type="text" class="form-control" placeholder="Name" id="name" required="" data-validation-required-message="Please enter your name." aria-invalid="false">
										<p class="help-block"></p>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row control-group">
									<div class="form-group col-xs-12 controls">
									  
										<input type="email" class="form-control" placeholder="Email Address" id="email" required="" data-validation-required-message="Please enter your email address.">
										<p class="help-block"></p>
									</div>
								</div> 
							</div>
						</div>
						<div class="row control-group">
							<div class="form-group col-xs-12 controls">
								
								<textarea rows="5" class="form-control" placeholder="Message" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
								<p class="help-block"></p>
							</div>
						</div>
					   
						<div id="success"></div>
						<div class="row">
							<div class="form-group col-xs-12">
								<button type="submit" class="btn btn-skin btn-lg">Send Message</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-6">
					<div id="map-canvas" style="width:100%; height: 300px;"></div>
					<div class="space-40"></div>
					<ul class="list-unstyled contact contact-info">
						<li><p><strong><i class="fa fa-map-marker"></i> Address:</strong> vaisahali, jaipur, 302012</p></li> 
						<li><p><strong><i class="fa fa-envelope"></i> Mail Us:</strong> <a href="#">Support@designmylife.com</a></p></li>
						<li> <p><strong><i class="fa fa-phone"></i> Phone:</strong> +91 1800 2345 2132</p></li>
						<li> <p><strong><i class="fa fa-print"></i> Fax:</strong> +91 2345 2132</p></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="space-60"></div>

<?php include 'assets/footer.php';?>
<?php include 'assets/footer-bottom.php';?>
<?php include 'assets/schema.php';?>
<?php echo $row["js"];?>
<script>
var myLatlng;
var map;
var marker;
function initialize() {
myLatlng = new google.maps.LatLng(37.397802, -121.890288);
var mapOptions = {
zoom: 13,
center: myLatlng,
mapTypeId: google.maps.MapTypeId.ROADMAP,
scrollwheel: false,
draggable: false
};
map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
var contentString = '<p style="line-height: 20px;"><strong>assan Template</strong></p><p>Vailshali, assan City, 302012</p>';
var infowindow = new google.maps.InfoWindow({
content: contentString
});
marker = new google.maps.Marker({
position: myLatlng,
map: map,
title: 'Marker'
});
google.maps.event.addListener(marker, 'click', function () {
infowindow.open(map, marker);
});
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php } mysqli_close($conn);} ?>
</body>
</html>