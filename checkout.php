<?php
require_once('assets/configuration.php');
$type = 'checkout';
$name = '';
$sql = "SELECT title,description,canonical,css,js FROM pages WHERE type = '$type' AND name = ''";
$result = $conn->query($sql);
if($result->num_rows > 0){
while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html><!-- 60201 -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $row["title"];?>
<?php echo $row["description"];?>
<?php echo $row["canonical"];?>
<?php echo $row["css"];?>
</head>
<body>
<?php include 'assets/search.php';?>
<header class="header">
<?php include 'assets/topbar.php';?>


			<!--main navigation start-->
			<!-- Static navbar -->
			<nav class="navbar navbar-default navbar-static-top yamm sticky">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt="logo"></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<!--mega menu-->
							<li class="dropdown active yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Herculiner Bed Liner<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Base pages</h3>
													<ul class="nav mega-vertical-nav">
														
														
														<li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
														<li><a href="register.php"><i class="fa fa-user"></i>Register</a></li>
														<li><a href="contact.php"><i class="fa fa-map-marker"></i>Contact</a></li>
														<li><a href="404.php"><i class="fa fa-trash-o"></i>404</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Product Pages </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="product-list.php"><i class="fa fa-list"></i> Product list</a></li>
														<li><a href="product-detail.php"><i class="fa fa-angle-right"></i> Product Detail </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Cart Pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
														<li><a href="checkout.php"><i class="fa fa-truck"></i> Checkout</a></li>   
														<li><a href="order-history.php"><i class="fa fa-sliders"></i> Order history </a></li>
														<li><a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/10.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<!--mega menu-->
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Blue DEF Equipment<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/8.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Peak 12v Electronics<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#">Nam ipsum est</a></li>
									<li><a href="#">Volutpat</a></li>
									<li><a href="#">In efficitur in</a></li>
									<li><a href="#">Accumsan eget</a></li>
									<li><a href="#">Curabitur</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Blog<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									
									<li><a href="blog-masonry.php">Masonry view</a></li>
									<li><a href="blog-post.php">Single Post</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="pe-7s-cart"></i><span class="badge">5</span></a>
								<div class="dropdown-menu shopping-cart">
									<div class="cart-items content-scroll">
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/1.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/4.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/2.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/3.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/6.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
									</div><!--cart-items-->
									<div class="cart-footer">
										<a href="#" class="btn btn-light-dark">View Cart</a>
										<a href="#" class="btn btn-skin">Checkout</a>
									</div><!--footer of cart-->
								</div><!--cart dropdown end-->
							</li>
						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			<!--main navigation end-->
		</header>
		<!--header end-->

		<!--breadcrumb start-->
		<div class="breadcrumb-wrapper">
			<div class="container">
				<h1>Checkout</h1>
			</div>
		</div>
		<!--end breadcrumb-->

		<div class="space-60"></div>
		<div class="container sky-checkout-form">
			
			<form action="" id="checkout-form" class="sky-form">
				<fieldset>	
					<div class="margin-b-20">
						<p>Returning customer? <a href="#"> Click here to login</a></p>
						<!--<p>Have a Coupon code?<a href="#"> Click here to Apply</a></p>-->
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input">
								<i class="icon-prepend fa fa-user"></i>
								<input type="text" name="fname" placeholder="First name">
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<i class="icon-prepend fa fa-user"></i>
								<input type="text" name="lname" placeholder="Last name">
							</label>
						</section>
					</div>
					<div class="row">
						<section class="col col-6">
							<label class="input">
								<i class="icon-prepend fa fa-envelope"></i>
								<input type="email" name="email" placeholder="E-mail">
							</label>
						</section>
						<section class="col col-6">
							<label class="input">
								<i class="icon-prepend fa fa-phone"></i>
								<input type="tel" name="phone" placeholder="Phone">
							</label>
						</section>
					</div>
				</fieldset>
				<fieldset>
					<div class="row">
						<section class="col col-5">
							<label class="select">
								<select name="country">
									<option value="0" selected disabled>Country</option>
									<option value="244">Aaland Islands</option>
									<option value="1">Afghanistan</option>
									<option value="2">Albania</option>
									<option value="3">Algeria</option>
									<option value="4">American Samoa</option>
									<option value="5">Andorra</option>
									<option value="6">Angola</option>
									<option value="7">Anguilla</option>
									<option value="8">Antarctica</option>
									<option value="9">Antigua and Barbuda</option>
									<option value="10">Argentina</option>
									<option value="11">Armenia</option>
									<option value="12">Aruba</option>
									<option value="13">Australia</option>
									<option value="14">Austria</option>
									<option value="15">Azerbaijan</option>
									<option value="16">Bahamas</option>
									<option value="17">Bahrain</option>
									<option value="18">Bangladesh</option>
									<option value="19">Barbados</option>
									<option value="20">Belarus</option>
									<option value="21">Belgium</option>
									<option value="22">Belize</option>
									<option value="23">Benin</option>
									<option value="24">Bermuda</option>
									<option value="25">Bhutan</option>
									<option value="26">Bolivia</option>
									<option value="245">Bonaire, Sint Eustatius and Saba</option>
									<option value="27">Bosnia and Herzegovina</option>
									<option value="28">Botswana</option>
									<option value="29">Bouvet Island</option>
									<option value="30">Brazil</option>
									<option value="31">British Indian Ocean Territory</option>
									<option value="32">Brunei Darussalam</option>
									<option value="33">Bulgaria</option>
									<option value="34">Burkina Faso</option>
									<option value="35">Burundi</option>
									<option value="36">Cambodia</option>
									<option value="37">Cameroon</option>
									<option value="38">Canada</option>
									<option value="251">Canary Islands</option>
									<option value="39">Cape Verde</option>
									<option value="40">Cayman Islands</option>
									<option value="41">Central African Republic</option>
									<option value="42">Chad</option>
									<option value="43">Chile</option>
									<option value="44">China</option>
									<option value="45">Christmas Island</option>
									<option value="46">Cocos (Keeling) Islands</option>
									<option value="47">Colombia</option>
									<option value="48">Comoros</option>
									<option value="49">Congo</option>
									<option value="50">Cook Islands</option>
									<option value="51">Costa Rica</option>
									<option value="52">Cote D'Ivoire</option>
									<option value="53">Croatia</option>
									<option value="54">Cuba</option>
									<option value="246">Curacao</option>
									<option value="55">Cyprus</option>
									<option value="56">Czech Republic</option>
									<option value="237">Democratic Republic of Congo</option>
									<option value="57">Denmark</option>
									<option value="58">Djibouti</option>
									<option value="59">Dominica</option>
									<option value="60">Dominican Republic</option>
									<option value="61">East Timor</option>
									<option value="62">Ecuador</option>
									<option value="63">Egypt</option>
									<option value="64">El Salvador</option>
									<option value="65">Equatorial Guinea</option>
									<option value="66">Eritrea</option>
									<option value="67">Estonia</option>
									<option value="68">Ethiopia</option>
									<option value="69">Falkland Islands (Malvinas)</option>
									<option value="70">Faroe Islands</option>
									<option value="71">Fiji</option>
									<option value="72">Finland</option>
									<option value="74">France, skypolitan</option>
									<option value="75">French Guiana</option>
									<option value="76">French Polynesia</option>
									<option value="77">French Southern Territories</option>
									<option value="126">FYROM</option>
									<option value="78">Gabon</option>
									<option value="79">Gambia</option>
									<option value="80">Georgia</option>
									<option value="81">Germany</option>
									<option value="82">Ghana</option>
									<option value="83">Gibraltar</option>
									<option value="84">Greece</option>
									<option value="85">Greenland</option>
									<option value="86">Grenada</option>
									<option value="87">Guadeloupe</option>
									<option value="88">Guam</option>
									<option value="89">Guatemala</option>
									<option value="241">Guernsey</option>
									<option value="90">Guinea</option>
									<option value="91">Guinea-Bissau</option>
									<option value="92">Guyana</option>
									<option value="93">Haiti</option>
									<option value="94">Heard and Mc Donald Islands</option>
									<option value="95">Honduras</option>
									<option value="96">Hong Kong</option>
									<option value="97">Hungary</option>
									<option value="98">Iceland</option>
									<option value="99">India</option>
									<option value="100">Indonesia</option>
									<option value="101">Iran (Islamic Republic of)</option>
									<option value="102">Iraq</option>
									<option value="103">Ireland</option>
									<option value="104">Israel</option>
									<option value="105">Italy</option>
									<option value="106">Jamaica</option>
									<option value="107">Japan</option>
									<option value="240">Jersey</option>
									<option value="108">Jordan</option>
									<option value="109">Kazakhstan</option>
									<option value="110">Kenya</option>
									<option value="111">Kiribati</option>
									<option value="113">Korea, Republic of</option>
									<option value="114">Kuwait</option>
									<option value="115">Kyrgyzstan</option>
									<option value="116">Lao People's Democratic Republic</option>
									<option value="117">Latvia</option>
									<option value="118">Lebanon</option>
									<option value="119">Lesotho</option>
									<option value="120">Liberia</option>
									<option value="121">Libyan Arab Jamahiriya</option>
									<option value="122">Liechtenstein</option>
									<option value="123">Lithuania</option>
									<option value="124">Luxembourg</option>
									<option value="125">Macau</option>
									<option value="127">Madagascar</option>
									<option value="128">Malawi</option>
									<option value="129">Malaysia</option>
									<option value="130">Maldives</option>
									<option value="131">Mali</option>
									<option value="132">Malta</option>
									<option value="133">Marshall Islands</option>
									<option value="134">Martinique</option>
									<option value="135">Mauritania</option>
									<option value="136">Mauritius</option>
									<option value="137">Mayotte</option>
									<option value="138">Mexico</option>
									<option value="139">Micronesia, Federated States of</option>
									<option value="140">Moldova, Republic of</option>
									<option value="141">Monaco</option>
									<option value="142">Mongolia</option>
									<option value="242">Montenegro</option>
									<option value="143">Montserrat</option>
									<option value="144">Morocco</option>
									<option value="145">Mozambique</option>
									<option value="146">Myanmar</option>
									<option value="147">Namibia</option>
									<option value="148">Nauru</option>
									<option value="149">Nepal</option>
									<option value="150">Netherlands</option>
									<option value="151">Netherlands Antilles</option>
									<option value="152">New Caledonia</option>
									<option value="153">New Zealand</option>
									<option value="154">Nicaragua</option>
									<option value="155">Niger</option>
									<option value="156">Nigeria</option>
									<option value="157">Niue</option>
									<option value="158">Norfolk Island</option>
									<option value="112">North Korea</option>
									<option value="159">Northern Mariana Islands</option>
									<option value="160">Norway</option>
									<option value="161">Oman</option>
									<option value="162">Pakistan</option>
									<option value="163">Palau</option>
									<option value="247">Palestinian Territory, Occupied</option>
									<option value="164">Panama</option>
									<option value="165">Papua New Guinea</option>
									<option value="166">Paraguay</option>
									<option value="167">Peru</option>
									<option value="168">Philippines</option>
									<option value="169">Pitcairn</option>
									<option value="170">Poland</option>
									<option value="171">Portugal</option>
									<option value="172">Puerto Rico</option>
									<option value="173">Qatar</option>
									<option value="174">Reunion</option>
									<option value="175">Romania</option>
									<option value="176">Russian Federation</option>
									<option value="177">Rwanda</option>
									<option value="178">Saint Kitts and Nevis</option>
									<option value="179">Saint Lucia</option>
									<option value="180">Saint Vincent and the Grenadines</option>
									<option value="181">Samoa</option>
									<option value="182">San Marino</option>
									<option value="183">Sao Tome and Principe</option>
									<option value="184">Saudi Arabia</option>
									<option value="185">Senegal</option>
									<option value="243">Serbia</option>
									<option value="186">Seychelles</option>
									<option value="187">Sierra Leone</option>
									<option value="188">Singapore</option>
									<option value="189">Slovak Republic</option>
									<option value="190">Slovenia</option>
									<option value="191">Solomon Islands</option>
									<option value="192">Somalia</option>
									<option value="193">South Africa</option>
									<option value="194">South Georgia &amp; South Sandwich Islands</option>
									<option value="248">South Sudan</option>
									<option value="195">Spain</option>
									<option value="196">Sri Lanka</option>
									<option value="249">St. Barthelemy</option>
									<option value="197">St. Helena</option>
									<option value="250">St. Martin (French part)</option>
									<option value="198">St. Pierre and Miquelon</option>
									<option value="199">Sudan</option>
									<option value="200">Suriname</option>
									<option value="201">Svalbard and Jan Mayen Islands</option>
									<option value="202">Swaziland</option>
									<option value="203">Sweden</option>
									<option value="204">Switzerland</option>
									<option value="205">Syrian Arab Republic</option>
									<option value="206">Taiwan</option>
									<option value="207">Tajikistan</option>
									<option value="208">Tanzania, United Republic of</option>
									<option value="209">Thailand</option>
									<option value="210">Togo</option>
									<option value="211">Tokelau</option>
									<option value="212">Tonga</option>
									<option value="213">Trinidad and Tobago</option>
									<option value="214">Tunisia</option>
									<option value="215">Turkey</option>
									<option value="216">Turkmenistan</option>
									<option value="217">Turks and Caicos Islands</option>
									<option value="218">Tuvalu</option>
									<option value="219">Uganda</option>
									<option value="220">Ukraine</option>
									<option value="221">United Arab Emirates</option>
									<option value="222">United Kingdom</option>
									<option value="223">United States</option>
									<option value="224">United States Minor Outlying Islands</option>
									<option value="225">Uruguay</option>
									<option value="226">Uzbekistan</option>
									<option value="227">Vanuatu</option>
									<option value="228">Vatican City State (Holy See)</option>
									<option value="229">Venezuela</option>
									<option value="230">Viet Nam</option>
									<option value="231">Virgin Islands (British)</option>
									<option value="232">Virgin Islands (U.S.)</option>
									<option value="233">Wallis and Futuna Islands</option>
									<option value="234">Western Sahara</option>
									<option value="235">Yemen</option>
									<option value="238">Zambia</option>
									<option value="239">Zimbabwe</option>
								</select>
								<i></i>
							</label>
						</section>
						<section class="col col-4">
							<label class="input">
								<input type="text" name="city" placeholder="City">
							</label>
						</section>
						<section class="col col-3">
							<label class="input">
								<input type="text" name="code" placeholder="Post code">
							</label>
						</section>
					</div>
					<section>
						<label for="file" class="input">
							<input type="text" name="address" placeholder="Address">
						</label>
					</section>
					<section>
						<label class="textarea">
							<textarea rows="3" name="info" placeholder="Additional info"></textarea>
						</label>
					</section>
				</fieldset>
				<fieldset>
					<section>
						<div class="inline-group">
							<label class="radio"><input type="radio" name="radio-inline" checked><i></i>Visa</label>
							<label class="radio"><input type="radio" name="radio-inline"><i></i>MasterCard</label>
							<label class="radio"><input type="radio" name="radio-inline"><i></i>PayPal</label>
						</div>
					</section>
					<section>
						<label class="input">
							<input type="text" name="name" placeholder="Name on card">
						</label>
					</section>
					<div class="row">
						<section class="col col-10">
							<label class="input">
								<input type="text" name="card" id="card" placeholder="Card number">
							</label>
						</section>
						<section class="col col-2">
							<label class="input">
								<input type="text" name="cvv" id="cvv" placeholder="CVV2">
							</label>
						</section>
					</div>
					<div class="row">
						<label class="label col col-4">Expiration date</label>
						<section class="col col-5">
							<label class="select">
								<select name="month">
									<option value="0" selected disabled>Month</option>
									<option value="1">January</option>
									<option value="1">February</option>
									<option value="3">March</option>
									<option value="4">April</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">August</option>
									<option value="9">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
								<i></i>
							</label>
						</section>
						<section class="col col-3">
							<label class="input">
								<input type="text" name="year" id="year" placeholder="Year">
							</label>
						</section>
					</div>
				</fieldset>
				<div class="space-30"></div>
				<div class="cart-total">
					<fieldset>
						<h2>Cart total</h2>
						<table class="table table-bordered">
							<tbody>
								<tr>
									<td>Subtotal</td>
									<td class="color">$2456.00</td>
								</tr>
								<tr>
									<td>Shipping</td>
									<td class="color">Free Shipping</td>
								</tr>
								<tr>
									<td>Total</td>
									<td class="total color">$2456.00</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
				</div>
				<div class="space-30"></div>
				<footer class="text-right">
					<button type="submit" class="btn btn-light-dark btn-lg">Place order</button>
				</footer>
			</form>
		</div>
		<div class="space-60"></div>

<?php include 'assets/footer.php';?>
<?php include 'assets/footer-bottom.php';?>
<?php include 'assets/schema.php';?>
<?php echo $row["js"];?>
<script>
$(function()
{
// Masking
$('#cvv').mask('999', {placeholder: 'X'});
$('#card').mask('9999-9999-9999-9999', {placeholder: 'X'});
$('#year').mask('2099', {placeholder: 'X'});
// Add validation method
$.validator.addMethod("creditcard", function (value,element,param){if(/[^0-9\-]+/.test(value)){return false;}},
$.validator.format('Please enter a valid credit card number.'));
// Validation
$('#checkout-form').validate(
{
// Rules for form validation
rules:{
fname:{required:true},
lname:{required:true},
email:{required:true,email:true},
phone:{required:true},
country:{required:true},
city:{required:true},
code:{required:true,digits:true},
address:{required:true},
name:{required:true},
card:{required:true},
cvv:{required:true,digits:true},
month:{required:true},
year:{required:true,digits:true}
},
// Messages for form validation
messages:{
fname:{required:'Please enter your first name'},
lname:{required:'Please enter your last name'},
email:{required:'Please enter your email address',email:'Please enter a VALID email address'},
phone:{required:'Please enter your phone number'},
country:{required:'Please select your country'},
city:{required:'Please enter your city'},
code:{required:'Please enter code',digits:'Digits only please'},
address:{required:'Please enter your full address'},
name:{required:'Please enter name on your card'},
card:{required:'Please enter your card number'},
cvv:{required:'Enter CVV2',digits:'Digits only'},
month:{required:'Select month'},
year:{required:'Enter year',digits:'Digits only please'}
},
// Do not change code below
errorPlacement:function(error,element){error.insertAfter(element.parent());}
});
});
</script>
<?php } mysqli_close($conn);} ?>
</body>
</html>