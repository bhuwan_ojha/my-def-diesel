<?php
require_once('assets/configuration.php');
$type = 'index';
$name = '';
$sql = "SELECT title,description,canonical,css,js FROM pages WHERE type = '$type' AND name = ''";
$result = $conn->query($sql);
if($result->num_rows > 0){
while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html><!-- 71420 -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $row["title"];?>
<?php echo $row["description"];?>
<?php echo $row["canonical"];?>
<?php echo $row["css"];?>
</head>
<body>
<?php include 'assets/search.php';?>
<header class="header">
<?php include 'assets/topbar.php';?>


			<!--main navigation start-->
			<!-- Static navbar -->
			<nav class="navbar navbar-default navbar-static-top yamm sticky">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt="logo"></a>
					</div>
					
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href="index.php">Home</a></li>
							<!--mega menu-->
							
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Herculiner Bed Liner<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Base pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
														<li><a href="register.php"><i class="fa fa-user"></i>Register</a></li>
														<li><a href="contact.php"><i class="fa fa-map-marker"></i>Contact</a></li>
														<li><a href="404.php"><i class="fa fa-trash-o"></i>404</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Product Pages </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="product-list.php"><i class="fa fa-list"></i> Product list</a></li>
														<li><a href="product-detail.php"><i class="fa fa-angle-right"></i> Product Detail </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Cart Pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
														<li><a href="checkout.php"><i class="fa fa-truck"></i> Checkout</a></li>
														<li><a href="order-history.php"><i class="fa fa-sliders"></i> Order history </a></li>
														<li><a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist </a></li>
													</ul>
												</div>
												<div class="col-sm-3"><img src="images/women/10.jpg" class="img-responsive" alt=""></div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							
							<!--mega menu end--> 
							<!--mega menu-->
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Blue DEF Equipment<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3"><img src="images/women/8.jpg" class="img-responsive" alt=""></div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							
							<!--mega menu end--> 
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" >Peak 12v Electronics<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
												<h3 class="heading">Sample title</h3>
									<li><a href="#">Nam ipsum est</a></li>
									<li><a href="#">Volutpat</a></li>
									<li><a href="#">In efficitur in</a></li>
									<li><a href="#">Accumsan eget</a></li>
									<li><a href="#">Curabitur</a></li>
									</div>
											<div class="col-sm-3"><img src="images/women/8.jpg" class="img-responsive" alt=""></div>
											</div>
											</div>
								</ul>
							</li>
							
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Blog<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="blog-masonry.php">Masonry view</a></li>
									<li><a href="blog-post.php">Single Post</a></li>
								</ul>
							</li>
							
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="pe-7s-cart"></i><span class="badge">5</span></a>
								<div class="dropdown-menu shopping-cart">
									<div class="cart-items content-scroll">
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/1.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/4.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/2.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/3.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/6.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
									</div><!--cart-items-->

									<div class="cart-footer">
										<a href="#" class="btn btn-light-dark">View Cart</a>
										<a href="#" class="btn btn-skin">Checkout</a>
									</div><!--footer of cart-->


								</div><!--cart dropdown end-->
							</li>

						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			<!--main navigation end-->
		</header>
		<!--header end-->



		<!--slider revolution 5 start-->
		<article class="content">
			<div class="rev_slider_wrapper">
				<!-- START REVOLUTION SLIDER 5.0 auto mode -->
				<div id="rev_slider" class="rev_slider" data-version="5.0">
					<ul>
						<!-- SLIDE  -->
						<li data-transition="fade">

							<!-- MAIN IMAGE -->
							<img src="images/bg/bg-1.jpg"  alt=""  width="1920" height="600">

							<!-- LAYER NR. 1 -->
							<div class="tp-caption slider-title tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="center" data-hoffset="0" 
								 data-y="top" data-voffset="200"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="500" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 >Summer sale
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption slider-caption tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="center" data-hoffset="0" 
								 data-y="top" data-voffset="280"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="800" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 >Upto 50% off on fashion
							</div>
							<!-- LAYER NR. 3 -->
							<div class="tp-caption slider-button tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="center" data-hoffset="0" 
								 data-y="top" data-voffset="320"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="1200" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 ><a href="#">Shop Now</a>
							</div>
						</li>

						<!-- SLIDE  -->
						<li data-transition="fade">

							<!-- MAIN IMAGE -->
							<img src="images/bg/bg-2.jpg"  alt=""  width="1920" height="600">

							<!-- LAYER NR. 1 -->
							<div class="tp-caption slider-title tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="right" data-hoffset="100" 
								 data-y="top" data-voffset="150"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="500" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 > Mans collection
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption slider-caption tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="right" data-hoffset="100" 
								 data-y="top" data-voffset="230"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="800" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 >Lorem ipsum dolor sit amet
							</div>
							<!-- LAYER NR. 3 -->
							<div class="tp-caption slider-button tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="right" data-hoffset="100" 
								 data-y="top" data-voffset="280"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="1200" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 ><a href="#">Shop Now</a>
							</div>
						</li>

						<!-- SLIDE  -->
						<li data-transition="fade">

							<!-- MAIN IMAGE -->
							<img src="images/bg/bg-3.jpg"  alt=""  width="1920" height="600">

							<!-- LAYER NR. 1 -->
							<div class="tp-caption slider-title tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="center" data-hoffset="0" 
								 data-y="top" data-voffset="200"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="500" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 >Enjoy the freedom
							</div>
							<!-- LAYER NR. 2 -->
							<div class="tp-caption slider-caption tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="center" data-hoffset="0" 
								 data-y="top" data-voffset="280"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="800" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 >Upto 50% off on fashion
							</div>
							<!-- LAYER NR. 3 -->
							<div class="tp-caption slider-button tp-resizeme"
								 data-transform_idle="o:1;"
								 data-x="center" data-hoffset="0" 
								 data-y="top" data-voffset="320"
								 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
								 data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
								 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
								 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
								 data-start="1200" 
								 data-splitin="none" 
								 data-splitout="none" 
								 data-responsive_offset="on" 
								 ><a href="#">Shop Now</a>
							</div>
						</li>
					</ul>
				</div><!-- END REVOLUTION SLIDER -->
			</div>
		</article>

		<!--slider revolution 5 end-->
		<div class="space-60"></div>
		<div class="blocks-main">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="blocks-col">
							<img src="images/bg/bg-2.jpg" alt="" class="img-responsive">
							<div class="block-overlay">
								<h4>Menswear</h4>
								<p>Summer sale upto 50% off</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="blocks-col">
							<img src="images/bg/bg-3.jpg" alt="" class="img-responsive">
							<div class="block-overlay">
								<h4>Accessories</h4>
								<p>Lorem ipsum dolor sit  </p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="blocks-col">
							<img src="images/bg/bg-1.jpg" alt="" class="img-responsive">
							<div class="block-overlay">
								<h4>Womens collection</h4>
								<p>Winter sale upto 40% off  </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--block main-->


		<div class="space-60"></div>


		<!--new arrivals-->
		<section class="new-arrivals">
			<div class="container">
				<h2 class="section-heading">New Arrivals</h2>

				<!--owl carousel-->
				<div class="row">
					<div id="owl-slider" class="col-md-12 owl-carousel owl-theme">
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/women/1.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Sky-Blue <br>Short Skirt</h5>
									<span class="price">$29.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/1.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Dark-Blue <br>Men's t-shirt</h5>
									<span class="price">$19.99 <del>$25.99</del></span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/women/2.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black <br>Short Skirt</h5>
									<span class="price">$29.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/3.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black <br>analog watch</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/4.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Backpack</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
					</div>
				</div>
				<!--owl end-->
			</div>
		</section>
		<!--end new arrivals-->
		<div class='space-60'></div>
		<!--parallax section start-->
		<section class="parallax">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center">
						<h1><span>Upto 50% off on men's collection</span></h1>
						<a href="#" class="btn btn-skin btn-lg">Shop Now</a>
					</div>
				</div>
			</div>
		</section>
		<!--parallax section end-->
		<div class='space-60'></div>

		<!--popular products-->
		<section class="featured-products">
			<div class="container">
				<h2 class="section-heading">Popular products</h2>
				<div class="row">
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/women/1.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder out-of-stock">
							<span class="out-stock-label">Out of stock</span>
							<a href="#"><img src="images/men/1.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/men/2.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/women/2.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
				</div><!--row-->

				<div class="row">
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/women/1.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<span class="offer-lablel">50% off</span>
							<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/men/7.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/men/4.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
				</div><!--row-->

				<div class="row">
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/women/8.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/men/3.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<a href="#"><img src="images/men/10.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
					<div class="col-sm-6 col-md-3 ">
						<div class="item_holder">
							<span class="label-new">New Arrived</span>
							<a href="#"><img src="images/women/10.jpg" alt="" class="img-responsive"></a>
							<div class="title">
								<h5>Sky-Blue <br>Short Skirt</h5>
								<span class="price">$29.99</span>
							</div>
						</div><!--item holder-->
					</div><!--col end-->
				</div><!--row-->
			</div>
		</section>
		<!--end Popular products-->
		<div class="space-50"></div>

		<div class="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<h3>Subscribe to newsletter</h3>
						<p>Lorem ipsum get latest update of products sit amet.</p>
					</div>
					<div class="col-md-7">
						<form role="form" method="post" action="#" class="subscribe-form  assan-newsletter">
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<input type="text" class="form-control" name="email" placeholder="Enter email to subscribe">
									</div>
								</div>
								<div class="col-sm-4 text-center">
									<div>
										<button class="newsletter-btn" name="submit" type="submit">Notify me</button>
									</div>  
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--newsletter end-->
		<div class="space-50"></div>
		<!--best sellers, featured, sale item section start-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<h3 class="bg-title">Top <span>Rated</span></h3>
					<div class="media-item clearfix">
						<img src="images/men/3.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Men's watch</a></h4>
							<span class="cat">Watches</span>
							<ul class="list-inline">
								<li><del>$49.99</del></li>
								<li>$29.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/men/4.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Men's Backpack</a></h4>
							<span class="cat">Backpack</span>
							<ul class="list-inline">
								<li><del>$99.99</del></li>
								<li>$69.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/women/8.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Women's T-shirt</a></h4>
							<span class="cat">T-shirts</span>
							<ul class="list-inline">
								<li><del>$49.99</del></li>
								<li>$29.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/women/6.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Yukute Lower</a></h4>
							<span class="cat">Lowers</span>
							<ul class="list-inline">
								<li><del>$29.99</del></li>
								<li>$19.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
				</div><!--col-end-->
				<div class="col-sm-4 col-xs-12">
					<h3 class="bg-title">new <span>arrivals</span></h3>
					<div class="media-item clearfix">
						<img src="images/men/3.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Men's watch</a></h4>
							<span class="cat">Watches</span>
							<ul class="list-inline">
								<li><del>$49.99</del></li>
								<li>$29.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/men/4.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Men's Backpack</a></h4>
							<span class="cat">Backpack</span>
							<ul class="list-inline">
								<li><del>$99.99</del></li>
								<li>$69.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/women/8.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Women's T-shirt</a></h4>
							<span class="cat">T-shirts</span>
							<ul class="list-inline">
								<li><del>$49.99</del></li>
								<li>$29.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/women/6.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Yukute Lower</a></h4>
							<span class="cat">Lowers</span>
							<ul class="list-inline">
								<li><del>$29.99</del></li>
								<li>$19.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
				</div><!--col-end-->
				<div class="col-sm-4 col-xs-12">
					<h3 class="bg-title">best <span>sellers</span></h3>
					<div class="media-item clearfix">
						<img src="images/men/9.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Men's T-shirt</a></h4>
							<span class="cat">T-shirts</span>
							<ul class="list-inline">
								<li><del>$49.99</del></li>
								<li>$29.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/women/3.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Women's Skirt</a></h4>
							<span class="cat">Skirts</span>
							<ul class="list-inline">
								<li><del>$99.99</del></li>
								<li>$69.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/women/4.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Women's Short</a></h4>
							<span class="cat">Shorts</span>
							<ul class="list-inline">
								<li><del>$49.99</del></li>
								<li>$29.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
					<div class="media-item clearfix">
						<img src="images/men/10.jpg" alt="" class="img-responsive" width="100">
						<div class="media-item-content">
							<h4><a href="#">Yukute Lower</a></h4>
							<span class="cat">Lowers</span>
							<ul class="list-inline">
								<li><del>$29.99</del></li>
								<li>$19.00</li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart"><i class="fa fa-shopping-cart"></i></a></li>
								<li><a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a></li>
							</ul>
							<div class='rating'>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-empty"></i>
							</div>
						</div>
					</div><!--media item-->
				</div><!--col-end-->
			</div>
		</div>
		<!--section end-->
		<div class="space-60"></div>
		<!--features section start-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 margin-b-30">
					<div class="features-box">
						<div class="icon">
							<i class="fa fa-truck"></i>
						</div>
						<div class="text">
							<h3>Free Shipping</h3>
							<p>Applies to the lower 48 state. Addition changes may apply to Canada, Hawaii, and Alaska.</p>
						</div>
					</div><!--features box-->
				</div><!--features column-->
				<div class="col-sm-4 margin-b-30">
					<div class="features-box blue">
						<div class="icon">
							<i class="fa fa-credit-card"></i>
						</div>
						<div class="text">
							<h3>Secure Payment</h3>
							<p>Payments handled by Paypal.</p>
						</div>
					</div><!--features box-->
				</div><!--features column-->
				<div class="col-sm-4 margin-b-30">
					<div class="features-box">
						<div class="icon">
							<i class="fa fa-plane"></i>
						</div>
						<div class="text">
							<h3>Expedited Shipping</h3>
							<p>May be available for additional charges... check the products description to see if it qualify's.</p>
						</div>
					</div><!--features box-->
				</div><!--features column-->
			</div>
		</div>
		<!--features section end-->
		<div class="space-30"></div>
		<!--partners-->
		<section class="partners">
			<div class='container'>
				<h2 class="section-heading">Our Partners</h2>
				<div class="row">
					<div id="owl-partners" class="col-md-12 owl-carousel owl-theme">
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
						<div class="item"><img src="images/partner.png" alt="parner" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</section>
		<!--end partners-->
		
<?php include 'assets/footer.php';?>
<?php include 'assets/footer-bottom.php';?>
<?php include 'assets/schema.php';?>
<?php echo $row["js"];?>
<?php } mysqli_close($conn);} ?>
</body>
</html>