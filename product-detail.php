<?php
require_once('assets/configuration.php');
$type = 'product';
$name = 'list';
$sql = "SELECT title,description,canonical,css,js FROM pages WHERE type = '$type' AND name = '$name'";
$result = $conn->query($sql);
if($result->num_rows > 0){
while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html><!-- 49111 -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $row["title"];?>
<?php echo $row["description"];?>
<?php echo $row["canonical"];?>
<?php echo $row["css"];?>
</head>
<body>
<?php include 'assets/search.php';?>
<header class="header">
<?php include 'assets/topbar.php';?>


			<!--main navigation start-->
			<!-- Static navbar -->
			<nav class="navbar navbar-default navbar-static-top yamm sticky">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt="logo"></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<!--mega menu-->
							<li class="dropdown active yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Herculiner Bed Liner<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="col-sm-3">
													<h3 class="heading">Base pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
														<li><a href="register.php"><i class="fa fa-user"></i>Register</a></li>
														<li><a href="contact.php"><i class="fa fa-map-marker"></i>Contact</a></li>
														<li><a href="404.php"><i class="fa fa-trash-o"></i>404</a></li>
													</ul>

												</div>
												<div class="col-sm-3">
													<h3 class="heading">Product Pages </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="product-list.php"><i class="fa fa-list"></i> Product list</a></li>
														<li><a href="product-detail.php"><i class="fa fa-angle-right"></i> Product Detail </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Cart Pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
														<li><a href="checkout.php"><i class="fa fa-truck"></i> Checkout</a></li>   
														<li><a href="order-history.php"><i class="fa fa-sliders"></i> Order history </a></li>
														<li><a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/10.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<!--mega menu-->
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Blue DEF Equipment<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>

												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/8.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Peak 12v Electronics<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#">Nam ipsum est</a></li>
									<li><a href="#">Volutpat</a></li>
									<li><a href="#">In efficitur in</a></li>
									<li><a href="#">Accumsan eget</a></li>
									<li><a href="#">Curabitur</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Blog<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">

									<li><a href="blog-masonry.php">Masonry view</a></li>
									<li><a href="blog-post.php">Single Post</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="pe-7s-cart"></i><span class="badge">5</span></a>
								<div class="dropdown-menu shopping-cart">

									<div class="cart-items content-scroll">
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/1.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/4.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/2.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/3.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/6.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
									</div><!--cart-items-->

									<div class="cart-footer">
										<a href="#" class="btn btn-light-dark">View Cart</a>
										<a href="#" class="btn btn-skin">Checkout</a>
									</div><!--footer of cart-->


								</div><!--cart dropdown end-->
							</li>

						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			<!--main navigation end-->
		</header>
		<!--header end-->

		<!--breadcrumb start-->
		<div class="breadcrumb-wrapper">
			<div class="container">
				<h1>Product Detail</h1>
			</div>
		</div>
		<!--end breadcrumb-->

		<div class="space-60"></div>
		<div class="container">
			<div class="row single-product">

				<div class="col-md-9">
					<div class="row">
						<div class="col-md-5 margin-b-30">
							<div id="product-single"  class="owl-carousel owl-theme single-product-slider">
								<div class="item">
									<a href="images/men/s-1.jpg" data-lightbox="roadtrip"> <img src="images/men/s-1.jpg" alt="Product image" class="img-responsive"></a>
								</div>
								<div class="item">
									<a href="images/men/s-2.jpg" data-lightbox="roadtrip"> <img src="images/men/s-2.jpg" alt="Product image" class="img-responsive"></a>
								</div>
								<div class="item">
									<a href="images/men/s-3.jpg" data-lightbox="roadtrip"> <img src="images/men/s-3.jpg" alt="Product image" class="img-responsive"></a>
								</div>
								<div class="item">
									<a href="images/men/s-4.jpg" data-lightbox="roadtrip"> <img src="images/men/s-4.jpg" alt="Product image" class="img-responsive"></a>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="product-detail-desc">
								<h3 class="title"><a href="#">Product Name</a></h3>
								<span class="price"><del>$299.00</del> $199.00</span>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-full"></i>
									<a href="#">8 Reviews</a>
								</span>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</p>
								<div class="colors">
									<span>Choose color</span>
									<a href="#" class="light"></a>
									<a href="#" class="blue"></a>
									<a href="#" class="yellow"></a>
									<a href="#" class="red"></a>
								</div>
								<div class="available">
									Availability : In Stock
								</div>
								<div class="size">
									<span>Size:</span> 
									<select><option>38</option><option>40</option><option>42</option><option>44</option></select>
								</div>
								<div class="add-buttons">
									<a href="#" class="btn btn-border btn-lg" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart" class="btn btn-skin btn-lg"><i class="fa fa-shopping-cart"></i> Add to cart</a>
								</div>
							</div>
						</div>
					</div><!--single product details end-->
					<div class="space-40"></div>
					<div class="row">
						<div class="col-md-12 item-more-info">
							<div>

								<!-- Nav tabs -->
								<ul class="nav nav-justified" role="tablist">
									<li role="presentation" class="active"><a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">Description</a></li>
									<li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Product Reviews</a></li>
									<li role="presentation"><a href="#questions-answers" aria-controls="questions-answers" role="tab" data-toggle="tab">Product Q & A</a></li>
									<li role="presentation"><a href="#documentation" aria-controls="documentation" role="tab" data-toggle="tab">Documentation</a></li>
									<li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Video's</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="desc">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
										</p>
										<p>
											Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst.
										</p>
									</div>


									<div role="tabpanel" class="tab-pane" id="reviews">
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>
													Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
												</p>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-empty"></i>
												</span>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>
													Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
												</p>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-empty"></i>
												</span>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>
													Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
												</p>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-empty"></i>
												</span>
											</div>
										</div><!--media-->
										<form role="form" method="post" action="#">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="reviewName" class="control-label">Name:<span class="text-error">*</span></label>
														<div><input type="text" class="form-control" id="reviewName"></div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="reviewEmail" class="control-label">Email:</label>
														<div><input type="text" class="form-control" id="reviewEmail"></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														<label for="review" class="control-label">Add Review:*</label>
														<textarea class="form-control" id="review">	</textarea>
													</div>
												</div>
											</div>
											<input type="submit" class="btn-skin btn btn-lg" value="Add Review">
										</form>
									</div>


									<div role="tabpanel" class="tab-pane" id="questions-answers">
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>Does Herculiner come in black?</p>
												<h5>Admin</h5>
												<p>Yes</p>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Dave</h5>
												<p>Does Herculiner use ground up tires?</p>
												<h5>Admin</h5>
												<p>LOL.... No! Virgin rubber granules.</p>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Mike</h5>
												<p>Will Herculiner bring back white?</p>
												<h5>Admin</h5>
												<p>Good question.... we are trying to get white back.</p>
											</div>
										</div><!--media-->
										<form role="form" method="post" action="#">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="questionName" class="control-label">Name:<span class="text-error">*</span></label>
														<div><input type="text" class="form-control" id="questionName"></div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="questionEmail" class="control-label">Email:<span class="text-error">*</span></label>
														<div><input type="text" class="form-control" id="questionEmail"></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														<label for="question" class="control-label">Submit Question:*</label>
														<textarea class="form-control" id="question">	</textarea>
													</div>
												</div>
											</div>
											<input type="submit" class="btn-skin btn btn-lg" value="Add Question">
										</form>
									</div>
									
									
									
									
									
									
									
									
									
									
									
									<div role="tabpanel" class="tab-pane" id="documentation">
										<p>
										</p>
									</div>
									
									<div role="tabpanel" class="tab-pane" id="videos">
										<p>
										</p>
									</div>
									
									
								</div>

							</div>
						</div>
					</div>

				</div>

				<div class="col-md-3">
					<div class="sidebar-widget">
						<h3>Categories</h3>
						<ul class="list-unstyled">
							<li><a href="#">New Arrivals</a></li>
							<li><a href="#">Men</a></li>
							<li><a href="#">Women</a></li>
							<li><a href="#">T-shirts</a></li>
							<li><a href="#">Shoes</a></li>
							<li><a href="#">Handbags</a></li>
							<li><a href="#">Accessories</a></li>
						</ul>
					</div><!--sidebar-widget end-->
					<div class="sidebar-widget">
						<h3>Bestseller </h3>
						<div class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="images/men/5.jpg" alt="" width="70">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href='#'>men's backpack</a></h4>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-empty"></i>
								</span>
								<span class="price">
									<del>$99.00</del>
									$49.00
								</span>

							</div>
						</div><!--media-->
						<div class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="images/men/6.jpg" alt="" width="70">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href='#'>men's T-shirts</a></h4>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-empty"></i>
								</span>
								<span class="price">
									<del>$99.00</del>
									$49.00
								</span>
							</div>
						</div><!--media-->
						<div class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="images/women/5.jpg" alt="" width="70">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href='#'>Women's lowers</a></h4>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-empty"></i>
								</span>
								<span class="price">
									<del>$99.00</del>
									$49.00
								</span>
							</div>
						</div><!--media-->
					</div><!--sidebar-widget end-->
					<div class="sidebar-widget clearfix">
						<h3>Color</h3>
						<a class="color-box gray" href='#'></a>
						<a class="color-box black" href='#'></a>
						<a class="color-box blue" href='#'></a>
						<a class="color-box red" href='#'></a>
						<a class="color-box yellow" href='#'></a>
					</div>
				</div><!--sidebar col-->
			</div>
			<div class="space-60"></div>
			<div class="similar-products">
				<h2 class="section-heading">Optional Products</h2>
				<!--owl carousel-->
				<div class="row">
					<div id="owl-slider" class="col-md-12 owl-carousel owl-theme">
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/women/1.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Sky-Blue <br>Short Skirt</h5>
									<span class="price">$29.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/1.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Dark-Blue <br>Men's t-shirt</h5>
									<span class="price">$19.99 <del>$25.99</del></span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/women/2.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black <br>Short Skirt</h5>
									<span class="price">$29.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/3.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black <br>analog watch</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/4.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Backpack</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
					</div>
				</div>
				<!--owl end-->
			</div><!--similar products-->

		</div>
		
		
		
		
		
		
		
<div class="space-60"></div>
<?php include 'assets/newsletter.php';?>
<?php include 'assets/footer.php';?>
<?php include 'assets/footer-bottom.php';?>
<?php include 'assets/schema.php';?>
<?php echo $row["js"];?>
<?php } mysqli_close($conn);} ?>
</body>
</html>