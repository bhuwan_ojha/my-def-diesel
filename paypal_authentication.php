<?php
$strGetValue = $_REQUEST['code'];
$strGetCodePaypalLoginDetails = $strGetValue;

if ($strGetCodePaypalLoginDetails != '') {
    // Change this curl_init url according to your environment.
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $ch = curl_init( "https://api.sandbox.paypal.com/v1/identity/openidconnect/tokenservice" );

    $flag_result = 1;

    curl_setopt_array( $ch,
        array(
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => 'client_id=AaWv_8mwS0uu83SVTd5Soa376Qyh_tcfJWlQCHMseyUdwM6Iej7SuW8s1xTxzNLZKpMjHr6yXpXmyQM3&client_secret=EIJlSOzBxPf31fk60H7Or7mhGp2m2DlbmaEtNnsRaFbjsHtH2KnkuW9BO3VOQC3X7BArfzsZ9qbkQHrB&grant_type=authorization_code&code='.$strGetCodePaypalLoginDetails,
            CURLOPT_RETURNTRANSFER => 1
        )
    );

    $arrResponse = curl_exec($ch);

    if ( $arrResponse === false ) {
        $arrResponseToken = 'Curl error: ' . curl_error($ch);
        $flag_result = 0;

    } else{

        //Operation completed without any errors
        $arrResponse = json_decode($arrResponse);
        $strAccess_Token = $arrResponse->{'access_token'};

        // Change this curl_init url according to your environment.
        $chToken = curl_init( "https://api.sandbox.paypal.com/v1/identity/openidconnect/userinfo/?schema=openid&access_token=" .$strAccess_Token );

        curl_setopt_array( $chToken,
            array(
                CURLOPT_RETURNTRANSFER => 1
            )
        );

        $arrResponseToken = curl_exec($chToken);

        if ( $arrResponseToken === false ) {
            $arrResponseToken = 'Curl error: ' . curl_error($chToken);
            $flag_result = 0;
        }
    }
}

/* I have added this details on session because I wanted to use that details into the another action.
As per your requirement you can use the array ($arrResponseToken).*/

$_SESSION['SET_PAYPAL_USER_DATA'] = $arrResponseToken;
$_SESSION['FLAG_RESULT']          = $flag_result;

print_r($_SESSION);

