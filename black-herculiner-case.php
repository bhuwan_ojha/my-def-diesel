<?php
require_once('assets/configuration.php');
$type = 'product';
$name = 'list';
$sql = "SELECT title,description,canonical,css,js FROM pages WHERE type = '$type' AND name = '$name'";
$result = $conn->query($sql);
if($result->num_rows > 0){
while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html><!-- 49111 -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $row["title"];?>
<?php echo $row["description"];?>
<?php echo $row["canonical"];?>
<?php echo $row["css"];?>
</head>
<body>
<?php include 'assets/search.php';?>
<header class="header">
<?php include 'assets/topbar.php';?>


			<!--main navigation start-->
			<!-- Static navbar -->
			<nav class="navbar navbar-default navbar-static-top yamm sticky">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt="logo"></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<!--mega menu-->
							<li class="dropdown active yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Herculiner Bed Liner<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Base pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
														<li><a href="register.php"><i class="fa fa-user"></i>Register</a></li>
														<li><a href="contact.php"><i class="fa fa-map-marker"></i>Contact</a></li>
														<li><a href="404.php"><i class="fa fa-trash-o"></i>404</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Product Pages </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="product-list.php"><i class="fa fa-list"></i> Product list</a></li>
														<li><a href="product-detail.php"><i class="fa fa-angle-right"></i> Product Detail </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Cart Pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
														<li><a href="checkout.php"><i class="fa fa-truck"></i> Checkout</a></li>   
														<li><a href="order-history.php"><i class="fa fa-sliders"></i> Order history </a></li>
														<li><a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist </a></li>
													</ul>
												</div>
												<div class="col-sm-3"><img src="images/women/10.jpg" class="img-responsive" alt=""></div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<!--mega menu-->
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Blue DEF Equipment<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3"><img src="images/women/8.jpg" class="img-responsive" alt=""></div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Peak 12v Electronics<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#">Nam ipsum est</a></li>
									<li><a href="#">Volutpat</a></li>
									<li><a href="#">In efficitur in</a></li>
									<li><a href="#">Accumsan eget</a></li>
									<li><a href="#">Curabitur</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Blog<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="blog-masonry.php">Masonry view</a></li>
									<li><a href="blog-post.php">Single Post</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="pe-7s-cart"></i><span class="badge">5</span></a>
								<div class="dropdown-menu shopping-cart">
									<div class="cart-items content-scroll">
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/1.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/4.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/2.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/3.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/6.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
									</div><!--cart-items-->

									<div class="cart-footer">
										<a href="#" class="btn btn-light-dark">View Cart</a>
										<a href="#" class="btn btn-skin">Checkout</a>
									</div><!--footer of cart-->


								</div><!--cart dropdown end-->
							</li>

						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			<!--main navigation end-->
		</header>
		<!--header end-->

		<!--breadcrumb start-->
		<div class="breadcrumb-wrapper">
			<div class="container">
				<h1>Product Detail</h1>
			</div>
		</div>
		<!--end breadcrumb-->

		<div class="space-60"></div>
		<div class="container">
			<div class="row single-product">

				<div class="col-md-9">
					<div class="row">
						<div class="col-md-5 margin-b-30">
							<div id="product-single"  class="owl-carousel owl-theme single-product-slider">
								<div class="item">
									<a href="images/men/s-1.jpg" data-lightbox="roadtrip"> <img src="images/men/s-1.jpg" alt="Product image" class="img-responsive"></a>
								</div>
								<div class="item">
									<a href="images/men/s-2.jpg" data-lightbox="roadtrip"> <img src="images/men/s-2.jpg" alt="Product image" class="img-responsive"></a>
								</div>
								<div class="item">
									<a href="images/men/s-3.jpg" data-lightbox="roadtrip"> <img src="images/men/s-3.jpg" alt="Product image" class="img-responsive"></a>
								</div>
								<div class="item">
									<a href="images/men/s-4.jpg" data-lightbox="roadtrip"> <img src="images/men/s-4.jpg" alt="Product image" class="img-responsive"></a>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="product-detail-desc">
								<h3 class="title"><a href="#">Product Name</a></h3>
								<span class="price"><del>$299.00</del> $199.00</span>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-full"></i>
									<a href="#">8 Reviews</a>
								</span>
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</p>
								<div class="colors">
									<span>Choose color</span>
									<a href="#" class="light"></a>
									<a href="#" class="blue"></a>
									<a href="#" class="yellow"></a>
									<a href="#" class="red"></a>
								</div>
								<div class="available">
									Availability : In Stock
								</div>
								<div class="size">
									<span>Size:</span> 
									<select><option>38</option><option>40</option><option>42</option><option>44</option></select>
								</div>
								<div class="add-buttons">
									<a href="#" class="btn btn-border btn-lg" data-toggle="tooltip" data-placement="top" title="Add to wishlist"><i class="fa fa-heart"></i></a>
									<a href="#" class="btn btn-border btn-lg"  data-toggle="tooltip" data-placement="top" title="Add to Compare"><i class="fa fa-random"></i></a>
									<a href="#" data-toggle="tooltip" data-placement="top" title="Add to cart" class="btn btn-skin btn-lg"><i class="fa fa-shopping-cart"></i> Add to cart</a>
								</div>
							</div>
						</div>
					</div><!--single product details end-->
					<div class="space-40"></div>
					<div class="row">
						<div class="col-md-12 item-more-info">
							<div>

								<!-- Nav tabs -->
								<ul class="nav nav-justified" role="tablist">
									<li role="presentation" class="active"><a href="#desc" aria-controls="desc" role="tab" data-toggle="tab">Description</a></li>
									<li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Product Reviews</a></li>
									<li role="presentation"><a href="#questions-answers" aria-controls="questions-answers" role="tab" data-toggle="tab">Product Q & A</a></li>
									<li role="presentation"><a href="#documentation" aria-controls="documentation" role="tab" data-toggle="tab">Documentation</a></li>
									<li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Video's</a></li>
								</ul>

								<!-- Tab panes -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="desc">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
										</p>
										<p>
											Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst.
										</p>
									</div>


									<div role="tabpanel" class="tab-pane" id="reviews">
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>
													Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
												</p>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-empty"></i>
												</span>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>
													Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
												</p>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-empty"></i>
												</span>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>
													Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
												</p>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-half-empty"></i>
												</span>
											</div>
										</div><!--media-->
										<form role="form" method="post" action="#">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="reviewName" class="control-label">Name:<span class="text-error">*</span></label>
														<div><input type="text" class="form-control" id="reviewName"></div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="reviewEmail" class="control-label">Email:</label>
														<div><input type="text" class="form-control" id="reviewEmail"></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														<label for="review" class="control-label">Add Review:*</label>
														<textarea class="form-control" id="review">	</textarea>
													</div>
												</div>
											</div>
											<input type="submit" class="btn-skin btn btn-lg" value="Add Review">
										</form>
									</div>


									<div role="tabpanel" class="tab-pane" id="questions-answers">
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Emily</h5>
												<p>Does Herculiner come in black?</p>
												<h5>Admin</h5>
												<p>Yes</p>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Dave</h5>
												<p>Does Herculiner use ground up tires?</p>
												<h5>Admin</h5>
												<p>LOL.... No! Virgin rubber granules.</p>
											</div>
										</div><!--media-->
										<div class="media">
											<div class="media-left"><a href="#"><img class="media-object img-circle" src="images/team-1.jpg" width="80" alt="..."></a></div>
											<div class="media-body">
												<h5>Mike</h5>
												<p>Will Herculiner bring back white?</p>
												<h5>Admin</h5>
												<p>Good question.... we are trying to get white back.</p>
											</div>
										</div><!--media-->
										<form role="form" method="post" action="#">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="questionName" class="control-label">Name:<span class="text-error">*</span></label>
														<div><input type="text" class="form-control" id="questionName"></div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="questionEmail" class="control-label">Email:<span class="text-error">*</span></label>
														<div><input type="text" class="form-control" id="questionEmail"></div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="form-group">
														<label for="question" class="control-label">Submit Question:*</label>
														<textarea class="form-control" id="question">	</textarea>
													</div>
												</div>
											</div>
											<input type="submit" class="btn-skin btn btn-lg" value="Add Question">
										</form>
									</div>
									
									
									
									
									
									
									
									
									
									
									
									<div role="tabpanel" class="tab-pane" id="documentation">
										<p>
										<p>
										<a href="https://my-def-diesel.com/external/pdf/Herculiner-Bed-Liner-Application-Guide.pdf" target="_blank">Herculiner Bed Liner Application Guide</a>
										</p>
										<p>
										<a href="https://my-def-diesel.com/external/pdf/Herculiner-Truck-Bed-Liner-MSDS.pdf" target="_blank">Herculiner Truck Bed Liner MSDS</a>
										</p>
									</div>
									
									<div role="tabpanel" class="tab-pane" id="videos">
										<p>
										<iframe width="560" height="315" src="https://www.youtube.com/embed/V9N6M_VCdpk" frameborder="0" allowfullscreen></iframe>
										</p>
										<p>
										<span>0:00 </span><span>hey I'm Joe Thomas and i'm here with another great Peak do-it-yourself video</span><br>
<span>0:04 </span><span>not think you're going to like this one today I'm going to show you how to apply</span><br>
<span>0:07 </span><span>the number one brand in roll on bed liners</span><br>
<span>0:10 </span><span>herculiner if you're watching this you probably already thought about</span><br>
<span>0:13 </span><span>installing a bed liner to protect your truck and maybe you said what the heck</span><br>
<span>0:18 </span><span>there's a spray on liner shop in town i'll just have them do it</span><br>
<span>0:22 </span><span>well let me fill you in on a little factoid herculiner is every bit as tough</span><br>
<span>0:26 </span><span>and strong as any spray on liner and it comes at a fraction of the price plus it</span><br>
<span>0:31 </span><span>can be installed in three easy steps</span><br>
<span>0:34 </span><span>so why wouldn't you do it yourself and no that's done right and save a load of</span><br>
<span>0:37 </span><span>cash doing it</span><br>
<span>0:39 </span><span>so let's get started first things first so pick up the herculiner kid at your</span><br>
<span>0:43 </span><span>local auto parts store</span><br>
<span>0:44 </span><span>it's got almost everything you need to get the job done including rollers a</span><br>
<span>0:48 </span><span>brush for tight spots and abrasive pad for prep work and of course a gallon of</span><br>
<span>0:53 </span><span>herculiner it's everything you need for a six foot truck bed and if you're doing</span><br>
<span>0:56 </span><span>an 8-footer just grab another quarter herculiner and you're good to go</span><br>
<span>1:00 </span><span>step one involves this abrasive pad and a little elbow grease</span><br>
<span>1:04 </span><span>let's get to it you want to start by scuffing up the surface where you put</span><br>
<span>1:08 </span><span>herculiner you want to get rid of all the glossy areas</span><br>
<span>1:11 </span><span>this allows the herculiner to bond and stick well to your truck bed</span><br>
<span>1:15 </span><span>make sure the surface is clean if you have to use a little soap or detergent</span><br>
<span>1:19 </span><span>it's all good just be sure to rinse everything down when you're done and</span><br>
<span>1:22 </span><span>make sure everything is dry before you move forward</span><br>
<span>1:25 </span><span>so next you want to remove your tailgate hardware and grab your masking tape</span><br>
<span>1:29 </span><span>you're going to want to mask off all areas where you don't want herculiner</span><br>
<span>1:33 </span><span>applied this includes the drain plugs and latch bolt holes after that's done</span><br>
<span>1:38 </span><span>you can go ahead and apply the solvent and this is where those rubber gloves</span><br>
<span>1:42 </span><span>come in handy</span><br>
<span>1:43 </span><span>you don't want to get any on you so we're also do this in a well-ventilated</span><br>
<span>1:47 </span><span>area so you don't get dizzy from the fumes</span><br>
<span>1:50 </span><span>this is strong stuff now once that's done you want to give it about 10</span><br>
<span>1:53 </span><span>minutes to let it dry and now the fun part starts</span><br>
<span>1:57 </span><span>we get to apply the herculiner but first you gotta stir up the herculiner really</span><br>
<span>2:01 </span><span>really well i use a drill with this paint mixing attachment if you don't</span><br>
<span>2:06 </span><span>have that then be sure to get a metal stir stick</span><br>
<span>2:10 </span><span>make sure to mix the thick layer on the bottom of the can to ensure uniform</span><br>
<span>2:14 </span><span>consistency see herculiner isn't a water based material</span><br>
<span>2:18 </span><span>it's polyurethane based which means it will stick to just about anything that's</span><br>
<span>2:22 </span><span>why you have to stir it up really good</span><br>
<span>2:29 </span><span>now it's time to get busy you want to start with the 2-inch brush that came</span><br>
<span>2:33 </span><span>with your herculiner kit and hit the places where the roller won't fit the</span><br>
<span>2:37 </span><span>best thing to do is dab it don't brush it start with a light coat but remember</span><br>
<span>2:42 </span><span>you don't want it to clump or dry after the seams and corners are all done now</span><br>
<span>2:47 </span><span>it's time to get busy with the roller start on the front and side panels so</span><br>
<span>2:51 </span><span>you don't paint yourself into a corner</span><br>
<span>2:53 </span><span>so once you've done all that you can start rolling on the truck that itself</span><br>
<span>2:56 </span><span>be general here</span><br>
<span>3:00 </span><span>there's no reason to lean into it just take your time</span><br>
<span>3:03 </span><span>nice even strokes if you get a little herculiner somewhere you didn't want it</span><br>
<span>3:08 </span><span>you can easily remove it while it's still wet with a little cleaning solvent</span><br>
<span>3:11 </span><span>so be sure to check everywhere</span><br>
<span>3:14 </span><span>so there you go coat one is now complete</span><br>
<span>3:17 </span><span>but you want to make sure you let it set up and dry for 1 24 hours or at least</span><br>
<span>3:21 </span><span>until it's no longer tacky</span><br>
<span>3:23 </span><span>so now it's time to apply the second coat</span><br>
<span>3:26 </span><span>if you look you can still see a little bit of yellow bleeding through but you</span><br>
<span>3:30 </span><span>need a second coat not just for looks but for durability</span><br>
<span>3:33 </span><span>so you're herculiner will last a long time start on the tape lines that way</span><br>
<span>3:38 </span><span>they'll drive first and be easier to remove recoat the entire bed just like</span><br>
<span>3:42 </span><span>we did in the first coat and we'll let it dry the same amount of time as the</span><br>
<span>3:45 </span><span>last be sure to remove the tape right after applying that second coat so it</span><br>
<span>3:50 </span><span>doesn't rip while peeling it off the bed</span><br>
<span>3:52 </span><span>once that's done check for any spots you may have missed</span><br>
<span>3:56 </span><span>just do a little touch-up it needs it and you are done just give it a chance</span><br>
<span>3:59 </span><span>to dry say 12 hours before light use in a day before heavy use</span><br>
<span>4:04 </span><span>there it is three simple steps and your truck bed is now protected by the most</span><br>
<span>4:08 </span><span>durable skid-resistant bed liner on the do-it-yourself market today and as a</span><br>
<span>4:13 </span><span>matter of fact it was so easy</span><br>
<span>4:14 </span><span>I bet you're thinking of other things you can use herculiner</span><br>
<span>4:17 </span><span>there are actually some great ideas on the website at herculiner . com</span><br>
<span>4:21 </span><span>plus you'll find a handy print out that you can refer to step by step when you</span><br>
<span>4:25 </span><span>apply herculiner to your truck bed</span><br>
<span>4:27 </span><span>hey that's it for me and remember do it yourself to make sure the job's done</span><br>
<span>4:31 </span><span>right and do it with herculiner to learn more about herculiner go to herculiner .</span><br>
<span>4:36 </span><span>com</span>
										</p>
									</div>
									
									
								</div>

							</div>
						</div>
					</div>

				</div>

				<div class="col-md-3">
					<div class="sidebar-widget">
						<h3>Categories</h3>
						<ul class="list-unstyled">
							<li><a href="#">New Arrivals</a></li>
							<li><a href="#">Men</a></li>
							<li><a href="#">Women</a></li>
							<li><a href="#">T-shirts</a></li>
							<li><a href="#">Shoes</a></li>
							<li><a href="#">Handbags</a></li>
							<li><a href="#">Accessories</a></li>
						</ul>
					</div><!--sidebar-widget end-->
					<div class="sidebar-widget">
						<h3>Bestseller </h3>
						<div class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="images/men/5.jpg" alt="" width="70">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href='#'>men's backpack</a></h4>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-empty"></i>
								</span>
								<span class="price">
									<del>$99.00</del>
									$49.00
								</span>

							</div>
						</div><!--media-->
						<div class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="images/men/6.jpg" alt="" width="70">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href='#'>men's T-shirts</a></h4>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-empty"></i>
								</span>
								<span class="price">
									<del>$99.00</del>
									$49.00
								</span>
							</div>
						</div><!--media-->
						<div class="media">
							<div class="media-left">
								<a href="#">
									<img class="media-object" src="images/women/5.jpg" alt="" width="70">
								</a>
							</div>
							<div class="media-body">
								<h4 class="media-heading"><a href='#'>Women's lowers</a></h4>
								<span class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-empty"></i>
								</span>
								<span class="price">
									<del>$99.00</del>
									$49.00
								</span>
							</div>
						</div><!--media-->
					</div><!--sidebar-widget end-->
					<div class="sidebar-widget clearfix">
						<h3>Color</h3>
						<a class="color-box gray" href='#'></a>
						<a class="color-box black" href='#'></a>
						<a class="color-box blue" href='#'></a>
						<a class="color-box red" href='#'></a>
						<a class="color-box yellow" href='#'></a>
					</div>
				</div><!--sidebar col-->
			</div>
			<div class="space-60"></div>
			<div class="similar-products">
				<h2 class="section-heading">Optional Products</h2>
				<!--owl carousel-->
				<div class="row">
					<div id="owl-slider" class="col-md-12 owl-carousel owl-theme">
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/women/1.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Sky-Blue <br>Short Skirt</h5>
									<span class="price">$29.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/1.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Dark-Blue <br>Men's t-shirt</h5>
									<span class="price">$19.99 <del>$25.99</del></span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/women/2.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black <br>Short Skirt</h5>
									<span class="price">$29.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/3.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black <br>analog watch</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/4.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Backpack</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
						<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
												<div class="item">
							<div class="item_holder">
								<a href="#"><img src="images/men/5.jpg" alt="" class="img-responsive"></a>
								<div class="title">
									<h5>Black & blue <br>Laptop bag</h5>
									<span class="price">$45.99</span>
								</div>
							</div><!--item holder-->
						</div> <!--item loop-->
					</div>
				</div>
				<!--owl end-->
			</div><!--similar products-->

		</div>
		
		
		
		
		
		
		
<div class="space-60"></div>
<?php include 'assets/newsletter.php';?>
<?php include 'assets/footer.php';?>
<?php include 'assets/footer-bottom.php';?>
<?php include 'assets/schema.php';?>
<?php echo $row["js"];?>
<?php } mysqli_close($conn);} ?>
</body>
</html>