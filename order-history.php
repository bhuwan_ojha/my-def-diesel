<?php
require_once('assets/configuration.php');
$type = 'history';
$name = '';
$sql = "SELECT title,description,canonical,css,js FROM pages WHERE type = '$type' AND name = ''";
$result = $conn->query($sql);
if($result->num_rows > 0){
while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html><!-- 33372 -->
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php echo $row["title"];?>
<?php echo $row["description"];?>
<?php echo $row["canonical"];?>
<?php echo $row["css"];?>
</head>
<body>
<?php include 'assets/search.php';?>
<header class="header">
<?php include 'assets/topbar.php';?>


			<!--main navigation start-->
			<!-- Static navbar -->
			<nav class="navbar navbar-default navbar-static-top yamm sticky">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt="logo"></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<!--mega menu-->
							<li class="dropdown active yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Herculiner Bed Liner<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="col-sm-3">
													<h3 class="heading">Base pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="login.php"><i class="fa fa-lock"></i>Login</a></li>
														<li><a href="register.php"><i class="fa fa-user"></i>Register</a></li>
														<li><a href="contact.php"><i class="fa fa-map-marker"></i>Contact</a></li>
														<li><a href="404.php"><i class="fa fa-trash-o"></i>404</a></li>
													</ul>

												</div>
												<div class="col-sm-3">
													<h3 class="heading">Product Pages </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="product-list.php"><i class="fa fa-list"></i> Product list</a></li>
														<li><a href="product-detail.php"><i class="fa fa-angle-right"></i> Product Detail </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Cart Pages</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="cart.php"><i class="fa fa-shopping-cart"></i> Cart</a></li>
														<li><a href="checkout.php"><i class="fa fa-truck"></i> Checkout</a></li>   
														<li><a href="order-history.php"><i class="fa fa-sliders"></i> Order history </a></li>
														<li><a href="wishlist.php"><i class="fa fa-heart"></i> Wishlist </a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/10.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<!--mega menu-->
							<li class="dropdown yamm-fw">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown">Blue DEF Equipment<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li>
										<div class="yamm-content">
											<div class="row">

												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>

												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title </h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<h3 class="heading">Sample title</h3>
													<ul class="nav mega-vertical-nav">
														<li><a href="#">Nam ipsum est</a></li>
														<li><a href="#">Volutpat</a></li>
														<li><a href="#">In efficitur in</a></li>
														<li><a href="#">Accumsan eget</a></li>
														<li><a href="#">Curabitur</a></li>
													</ul>
												</div>
												<div class="col-sm-3">
													<img src="images/women/8.jpg" class="img-responsive" alt="">
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li> <!--menu Features li end here-->
							<!--mega menu end--> 
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Peak 12v Electronics<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="#">Nam ipsum est</a></li>
									<li><a href="#">Volutpat</a></li>
									<li><a href="#">In efficitur in</a></li>
									<li><a href="#">Accumsan eget</a></li>
									<li><a href="#">Curabitur</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true">Blog<i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu">
									
									<li><a href="blog-masonry.php">Masonry view</a></li>
									<li><a href="blog-post.php">Single Post</a></li>
								</ul>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle js-activated" data-toggle="dropdown" role="button" aria-haspopup="true"><i class="pe-7s-cart"></i><span class="badge">5</span></a>
								<div class="dropdown-menu shopping-cart">

									<div class="cart-items content-scroll">
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/1.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/4.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/2.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/3.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
										<div class="cart-item clearfix">
											<div class="img">
												<img src="images/men/6.jpg" alt="" class="img-responsive">
											</div><!--img-->
											<div class="description">
												<a href="#">Mauris et ligula quis</a><strong class="price">1 x $44.95</strong>
											</div><!--Description-->
											<div class="buttons">
												<a href="#" class="fa fa-pencil"></a><a href="#" class="fa fa-trash-o"></a>
											</div>
										</div><!--cart item-->
									</div><!--cart-items-->

									<div class="cart-footer">
										<a href="#" class="btn btn-light-dark">View Cart</a>
										<a href="#" class="btn btn-skin">Checkout</a>
									</div><!--footer of cart-->


								</div><!--cart dropdown end-->
							</li>

						</ul>
					</div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			</nav>
			<!--main navigation end-->
		</header>
		<!--header end-->

		<!--breadcrumb start-->
		<div class="breadcrumb-wrapper">
			<div class="container">
				<h1>order History</h1>
			</div>
		</div>
		<!--end breadcrumb-->

		<div class="space-60"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-8">
					<form class="search-form">
						<input type="text" class="form-control" placeholder="Search Order">
						<i class="fa fa-search"></i>
					</form>
				</div>
			</div>
			<div class="table-responsive table-order-history">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th></th>
							<th>Product Name</th>
							<th>Order No.</th>
							<th>Status</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td valign="middle"><img src="images/men/1.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/2.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/3.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/4.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-warning"><i class="fa fa-refresh"></i></span> Return Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/5.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/6.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-danger"><i class="fa fa-times"></i></span> Cancel</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/7.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/8.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/9.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
						 <tr>
							<td valign="middle"><img src="images/men/10.jpg" alt="" width="50"></td>
							<td valign="middle">T-shirt</td>
							<td valign="middle">45985448</td>
							<td><span class="label-success"><i class="fa fa-check"></i></span> Success</td>
							<td>$99.00</td>
							<td class="total-order">$99.00</td>
						</tr>
					</tbody>
				</table>
			</div>
			<nav>
						<ul class="pagination pull-right clearfix">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
		</div>


<div class="space-60"></div>
<?php include 'assets/newsletter.php';?>
<?php include 'assets/footer.php';?>
<?php include 'assets/footer-bottom.php';?>
<?php include 'assets/schema.php';?>
<?php echo $row["js"];?>
<?php } mysqli_close($conn);} ?>
</body>
</html>