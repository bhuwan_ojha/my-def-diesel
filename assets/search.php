<div class="search-bar">
<div class="container">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">
<form>
<input type="text" class="form-control" placeholder="E.g. Black Herculiner...">
<span class="search-close"><i class="fa fa-times"></i></span>
</form>
</div>
</div>
</div>
</div>