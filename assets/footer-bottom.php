
<div class="footer-bottom">
<div class="container text-center">
<ul class="list-inline" itemscope itemtype="http://www.schema.org/SiteNavigationElement">
<li itemprop="name"><a itemprop="url" href="sitemap.php">Site Map</a></li>
<li itemprop="name"><a itemprop="url" href="about.php">About</a></li>
<li itemprop="name"><a itemprop="url" href="contact.php">Contact</a></li>
<li itemprop="name"><a itemprop="url" href="blog.php">Blog</a></li>
<li itemprop="name"><a itemprop="url" href="returns.php">Returns</a></li>
<li itemprop="name"><a itemprop="url" href="policy.php">Privacy Policy</a></li>
</ul>
<span class="copyright">&copy; Copyright <?php echo date("Y");?>, My DEF Diesel</span>
</div>
</div>