
<div class="top-bar">
<div class="container">
<div class="row">
<div class="col-sm-6 hidden-xs"><span>Welcome to our website!</span></div>
<div class="col-sm-6 text-right">
<ul class="list-inline">
<li class="hidden-xs"><a href="register.php"><i class="pe-7s-user"></i>Register</a></li>
<li><a href="login.php"><i class="pe-7s-lock"></i>Login</a></li>
<li><a href="javascript:void(0)" class="search-toggle"><i class="fa fa-search"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
