
<footer class="footer">
<div class="container">
<div class="row">
<div class="col-md-4 margin-b-30">
<h3>Product Tags</h3>
<div class="tags clearfix">
<a href="#">Fashion</a>
<a href="#">Shoes</a>
<a href="#">bootstrap</a>
<a href="#">Kids</a>
<a href="#">Slippers</a>
<a href="#">inner-wears</a>
<a href="#">Women</a>
<a href="#">Men</a>
<a href="#">T-shirts</a>
<a href="#">Jeans</a>
<a href="#">Socks</a>
<a href="#">Sports</a>
<a href="#">Clothes</a>
<a href="#">Watches</a>
</div>
</div>
<div class="col-md-4 margin-b-30">
<h3>Customer Service</h3>
<div class="media">
<div class="media-left"><i class="fa fa-building"></i></div>
<div class="media-body"><span class="media-heading">My DEF Diesel</span></div>
</div>
<div class="media">
<div class="media-left"><i class="fa fa-phone"></i></div>
<div class="media-body"><span class="media-heading">847-287-8920</span></div>
</div>
<div class="media">
<div class="media-left"><i class="fa fa-envelope"></i></div>
<div class="media-body"><span class="media-heading">sales@my-def-diesel.com</span></div>
</div>
<div class="media">
<div class="media-left"><i class="fa fa-home"></i></div>
<div class="media-body"><span class="media-heading">4969 Prairie Oak Road Gurnee, IL 60031</span></div>
</div>
<div class="media">
<div class="media-left"><i class="fa fa-clock-o"></i></div>
<div class="media-body"><span class="media-heading">Mon-Sat 9am - 5pm</span></div>
</div>
</div>
<div class="col-md-4 margin-b-30">
<h3>About store</h3>
<p>Looking to build strong lastings bonds & service the Herculiner Bed Liner, Blue DEF Diesel Exhaust Fluid, and Peak 12v Electronic products community.</p>
<ul class="list-inline social-footer">
<li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Facebook"><i class="fa fa-facebook"></i></a></li>
<li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Twitter"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Google plus"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Youtube"><i class="fa fa-youtube-play"></i></a></li>
<li><a href="#" data-toggle="tooltip" data-placement="top" data-title="Rss feeds"><i class="fa fa-rss"></i></a></li>
</ul>
</div>
</div>
</div>
</footer>