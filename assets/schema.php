
<script type="application/ld+json">
{
"@context":"http://schema.org",
"@type":"WebSite", 
"name":"My DEF Diesel",
"url":"https://my-def-diesel.com/",
"potentialAction":{"@type":"SearchAction","target":"https://my-def-diesel.com/?s={search_term}","query-input":"required name=search_term"}
}
</script>
<script type="application/ld+json">
{
"@context":"https://schema.org",
"@type":"Organization",
"name":"My DEF Diesel",
"legalName":"Coderneeded Corp",
"description":"Looking to build strong lasting bonds & service the Herculiner Bed Liner, Blue DEF Diesel Exhaust Fluid, and Peak 12v electronic products community.",
"logo":"https://my-def-diesel.com/Logo.png",
"url":"https://my-def-diesel.com/",
"brand":[{"@type":"Thing","name":"Herculiner","description":"#1 Selling DIY Bed liner Roll-On Kit."},
{"@type":"Thing","name":"Blue DEF","description":"Industry leader in DEF (Diesel Exhaust Fluid) liquid, pumps, and handling/transfer equipment."},
{"@type":"Thing","name":"Peak","description":"Leader in 12v automotive battery jump starters, battery chargers, backup camera, dash camera, inflators, mobile power outlets, and lights"}],
"contactPoint":[{"@type":"ContactPoint","telephone":"+1-847-287-8920","contactType":"Customer Service"}],
"telephone":"+1-847-287-8920",
"email":"sales(at)my-def-diesel.com",
"address":{"@type":"PostalAddress","streetAddress":"4969 Prairie Oak Road","addressLocality":"Gurnee","addressRegion":"IL","postalCode":"60031","addressCountry":"US"},
"areaServed":{"@type":"Country","name":["US","CA"]},
"logo":"https://my-def-diesel.com/logo.png",
"sameAs":["https://www.facebook.com/my-def-diesel","https://www.twitter.com/my-def-diesel","https://plus.google.com/+my-def-diesel","https://www.youtube.com/user/my-def-diesel","https://www.linkedin.com/company/my-def-diesel"]
}
</script>
<script type="application/ld+json">
{
"@context":"https://schema.org",
"@type":"LocalBusiness",
"name":"My DEF Diesel",
"url":"https://my-def-diesel.com/",
"image":"https://my-def-diesel.com/hello.jpg",
"description":"Looking to build bonds & service the Herculiner Bed Liner, Blue DEF Diesel Exhaust Fluid, and Peak 12v electronic products community.",
"telephone":"+1-847-287-8920",
"priceRange":"$20 - $10,000",
"address":{"@type":"PostalAddress","streetAddress":"4969 Prairie Oak Road","addressLocality":"Gurnee","addressRegion":"IL","postalCode":"60031"},
"paymentAccepted":"Paypal",
"currenciesAccepted":"USD",
"openingHours":"Mo-Sa 9:00-17:00"
}
</script>
